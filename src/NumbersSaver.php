<?php
use Console\EuroMillionsScrapper;
use Console\LottoEuroJackpotScrapper;
use Console\LottoScrapper;

class NumbersSaver
{
    public function getLotteryNumbersAndSaveJson()
    {
        $ems = new EuroMillionsScrapper();
        $lotto = new LottoScrapper();
        $lottoJackpot = new LottoEuroJackpotScrapper();

        $numbers = array_merge($ems->getResults(), $lotto->getResults(), $lottoJackpot->getResults());

        $this->saveNumbersToFile($numbers);
    }

    /**
     * Metoda zapisująca dane do pliku .json
     * @param array $numbers
     */
    public function saveNumbersToFile($numbers)
    {
        $fp = fopen('results.json', 'w');
        fwrite($fp, json_encode($numbers));
        fclose($fp);
    }
}