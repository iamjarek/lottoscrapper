<?php

abstract class Scrapper
{
    protected $regularNumbers = array();
    protected $results = array();
    protected $extraNumbers = array();
    protected $extraNumberQueries = array();
    protected $regularNumbersQuery = '';
    private $serviceUrl;
    private $xpath = null;

    public function __construct()
    {
        if (!isset($this->serviceUrl)) {
            throw new Exception('No website url given');
        }
    }

    public function getResults()
    {
        $regularNumbers = $this->getRegularNumbers();
        $extraNumbers = $this->getExtraNumbers();

        if (!empty($extraNumbers)) {
            //TODO Do przemyślenia te zagnieżdżenia
            foreach ($extraNumbers as $numberName => $resultsForService) {
                foreach ($resultsForService as $serviceName => $resultsSet) {
                    foreach ($resultsSet as $date => $lotteryData) {
                        foreach ($lotteryData as $lotteryId => $lotteryNumbers) {
                            foreach ($lotteryNumbers as $number) {
                                $regularNumbers[$serviceName][$date][$lotteryId][$numberName] = $number;
                            }
                        }
                    }
                }
            }
        }
        return $regularNumbers;
    }

    public function getRegularNumbers()
    {
        if (!isset($this->xpath)) {
            if (!$this->scrapeAndSetXpath()) {
                throw new Exception('scrapping didnt work');
            }
        }
        $query = $this->getRegularNumbersQuery();
        if ('' != $query) {
            $numbers = $this->getNumbersFromNode($this->getXPath()->query($query));
            $this->setRegularNumbers($numbers);
        }

        return $this->regularNumbers;
    }

    public function scrapeAndSetXpath()
    {
        $html = $this->scrape();
        if ('' == $html) {
            return false;
        }
        $dom = new DOMDocument();
        @$dom->loadHTML($html); //uciszone tylko na potrzeby "proof of concept"
        $this->setXPath($dom);

        return true;
    }

    protected function scrape()
    {
        if (!function_exists('curl_init')) {
            throw new Exception('cURL is not installed');
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->getServiceUrl());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($curl);
        curl_close($curl);

        return $output;
    }

    /**
     * @return string
     */
    public function getServiceUrl()
    {
        return $this->serviceUrl;
    }

    /**
     * @param $serviceUrl
     */
    protected function setServiceUrl($serviceUrl)
    {
        $this->serviceUrl = $serviceUrl;

    }

    private function getRegularNumbersQuery()
    {
        return $this->regularNumbersQuery;
    }

    /**
     * @param DOMNodeList $domNumbersList
     * @return array
     */
    public function getNumbersFromNode($domNumbersList)
    {
        $numbers = array();
        /** @var DOMElement $domNumber */
        foreach ($domNumbersList as $domNumber) {
            $numbers[] = $domNumber->nodeValue;
        }
        return $numbers;
    }

    /**
     * @return DOMXPath
     * @throws Exception
     */
    public function getXPath()
    {
        if (!isset($this->xpath)) {
            throw new Exception('xpath empty');
        }
        return $this->xpath;
    }

    /**
     * @param DOMDocument $domDocument
     */
    public function setXPath($domDocument)
    {
        $xpath = new DOMXPath($domDocument);
        $this->xpath = $xpath;
    }

    /**
     * @param $winningNumbers
     */
    private function setRegularNumbers(array $winningNumbers)
    {
        $this->regularNumbers = $winningNumbers;
    }

    protected function getExtraNumbers()
    {
        if (empty($this->extraNumbers)) {
            $extraNumberQueries = $this->getExtraNumbersQueries();
            foreach ($extraNumberQueries as $numberName => $extraNumbersQuery) {
                $extraNumbers[$numberName] = $this->getNumbersFromNode($this->getXPath()->query($extraNumbersQuery));
            }
            if (!empty($extraNumbers)) {
                $this->setExtraNumbers($extraNumbers);
            }
        }

        return $this->extraNumbers;
    }

    private function getExtraNumbersQueries()
    {
        return $this->extraNumberQueries;
    }

    /**
     * @param $extraNumbers
     */
    private function setExtraNumbers(array $extraNumbers)
    {
        $this->extraNumbers = $extraNumbers;
    }
}