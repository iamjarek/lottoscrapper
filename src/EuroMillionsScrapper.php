<?php namespace Console;

include_once 'Scrapper.class.php';
use DateTime;
use Scrapper;

class EuroMillionsScrapper extends Scrapper
{
    protected $extraNumberQueries = array(
        'e1' => '//div[@class="esp first"]/span[@class="int-num"]',
        'e2' => '//div[@class="esp"]/span[@class="int-num"]'
    );
    protected $regularNumbersQuery = '//div[@class="num"]/span[@class="int-num"]';
    private $serviceUrl = 'https://www.elgordo.com/results/euromillonariaen.asp';

    public function __construct()
    {
        $this->setServiceUrl($this->serviceUrl);
    }

    /**
     * @param DOMNodeList $domNumbersList
     * @return array
     */
    public function getNumbersFromNode($domNumbersList)
    {
        $numbers = array();
        $dateNode = $this->getXPath()->query('//div[@class="body_game ee"]/div[@class="c"]')[0]->textContent;
        /** @var DOMElement $domNumber */
        $date = trim(str_replace(chr(194) . chr(160), ' ', explode(',', $dateNode)[1]));
        $date = DateTime::createFromFormat('d M Y', $date)->format('Y-m-d');
        $lotteryId = 0;
        foreach ($domNumbersList as $domNumber) {
            $numbers['EuroMillions'][$date][$lotteryId][] = $domNumber->nodeValue;
        }

        return $numbers;
    }
}