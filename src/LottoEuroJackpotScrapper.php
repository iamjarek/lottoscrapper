<?php namespace Console;

use DateTime;
use DOMElement;
use Scrapper;

class LottoEuroJackpotScrapper extends Scrapper
{
    protected $extraNumberQueries = array();
    protected $regularNumbersQuery = '//tr[@class="wynik"]';
    private $serviceUrl = 'http://www.lotto.pl/eurojackpot/wyniki-i-wygrane';

    public function __construct()
    {
        $this->setServiceUrl($this->serviceUrl);
    }

    /**
     * @param DOMNodeList $domNumbersList
     * @return array
     */
    public function getNumbersFromNode($domNumbersList)
    {
        $numbers = array();
        /** @var DOMElement $domNumber */
        foreach ($domNumbersList as $domNumber) {
            $extraNumbersReached = false;
            $lotteryId = $domNumber->childNodes[0]->nodeValue;
            $date = DateTime::createFromFormat('d-m-y',
                strstr($domNumber->childNodes[1]->nodeValue, ',', true))->format('Y-m-d');
            foreach ($domNumber->childNodes[2]->childNodes[0]->childNodes[0]->childNodes as $number) {
                if ('number advantageNumber text-center' == $number->getAttribute('class')) {
                    $extraNumbersReached = true;
                    continue;
                }
                if ($extraNumbersReached) {
                    $numbers['EuroJackpot'][$date][$lotteryId]['extra'][] = $number->nodeValue;
                } else {
                    $numbers['EuroJackpot'][$date][$lotteryId]['regular'][] = $number->nodeValue;
                }
            }
        }

        return $numbers;
    }
}