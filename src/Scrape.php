<?php namespace Console;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Author: Jarosław Kiraga <iamjarek@gmail.com>
 */
class Scrape extends Command
{

    public function configure()
    {
        $this->setName('scrape')
            ->setDescription('Scrapes 3 websites for winning lottery numbers and saves them to json file')
            ->setHelp('This command allows you to scrape websites and save numbers as json');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->go($input, $output);
    }
}