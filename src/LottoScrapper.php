<?php namespace Console;

use DateTime;
use Scrapper;

class LottoScrapper extends Scrapper
{
    protected $extraNumberQueries = array();
    protected $regularNumbersQuery = '//tr[@class="wynik"]';
    private $serviceUrl = 'http://www.lotto.pl/lotto/wyniki-i-wygrane';

    public function __construct()
    {
        $this->setServiceUrl($this->serviceUrl);
    }

    /**
     * @param DOMNodeList $domNumbersList
     * @return array
     */
    public function getNumbersFromNode($domNumbersList)
    {
        $numbers = array();
        /** @var DOMElement $domNumber */
        foreach ($domNumbersList as $domNumber) {
            $lotteryId = $domNumber->childNodes[1]->nodeValue;
            $lotteryName = $domNumber->childNodes[0]->childNodes[0]->getAttribute('alt');
            $date = DateTime::createFromFormat('d-m-y',
                strstr($domNumber->childNodes[2]->nodeValue, ',', true))->format('Y-m-d');
            foreach ($domNumber->childNodes[3]->childNodes[0]->childNodes[0]->childNodes as $number) {
                $numbers[$lotteryName][$date][$lotteryId][] = $number->nodeValue;
            }
        }

        return $numbers;
    }
}