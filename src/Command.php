<?php namespace Console;

include_once 'NumbersSaver.php';
use NumbersSaver;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Author: Jarosław Kiraga <iamjarek@gmail.com>
 */
class Command extends SymfonyCommand
{

    public function __construct()
    {
        parent::__construct();
    }

    protected function go(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            '====**** Lottery Winning Numbers Scrapper ****====',
            '==========================================',
            '',
        ]);

        // outputs a message without adding a "\n" at the end of the line
        $output->write($this->scrape());
    }

    /**
     *
     */
    private function scrape()
    {
        $numbersSaver = new NumbersSaver;
        $numbersSaver->getLotteryNumbersAndSaveJson();

        return 'Zrobione, wygrane numery zapisane w pliku "results.json"';
    }
}