/** console.php **/
#!/usr/bin/env php
<?php
require_once __DIR__ . '/vendor/autoload.php';
use Console\Scrape;
use Symfony\Component\Console\Application;

/**
 * Author: Chidume Nnamdi <kurtwanger40@gmail.com>
 */
$app = new Application('Console App', 'v1.0.0');
$app->add(new Scrape());
$app->run();